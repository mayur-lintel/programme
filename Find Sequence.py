def checkio(matrix):
	for i in range(0,len(matrix)-1):
		hor_cnt = 0
		ver_cnt = 0
		for j in range(0,len(matrix[i])-1):
			if matrix[i][j] == matrix[i][j+1]:
				hor_cnt = hor_cnt + 1
			else:
				if hor_cnt > 2:
					return True
				else:
					hor_cnt = 0

			if matrix[j][i] == matrix[j+1][i]:
				ver_cnt = ver_cnt + 1
			else:
				if ver_cnt > 2:
					return TrueN)
				else:
					ver_cnt = 0
			if(i < len(matrix)-3 and j < len(matrix[i])-3):
				left_cross_cnt = 0
				for k in range(0,3):
					if matrix[i+k][j+k] == matrix[i+k+1][j+k+1]:
						left_cross_cnt = left_cross_cnt + 1
					else:
						break
			
				if left_cross_cnt > 2:
					return True

				right_cross_cnt = 0
				for k in range(1,4):
					if matrix[i+k-1][len(matrix[i])-k-j] == matrix[i+k][len(matrix[i])-k-1-j]:
						right_cross_cnt = right_cross_cnt + 1
					else:
						break
			
			if right_cross_cnt > 2:
				return True
		
		if hor_cnt > 2:
			return True
		if ver_cnt > 2:
			return True		
	if hor_cnt > 2 or ver_cnt > 2:
		return True
	else:
		return False

print(checkio([[1,9,7,8,9,3,6,5,6,2],[4,9,4,8,3,4,8,8,5,9],[2,8,5,5,7,8,6,1,3,6],[6,4,7,6,9,1,4,5,7,8],[4,7,7,9,8,8,8,8,4,4],[3,7,3,2,1,9,1,8,9,1],[4,7,2,4,8,1,2,3,6,2],[4,4,1,3,3,3,9,2,6,7],[8,6,1,9,3,5,8,1,7,5],[7,3,6,5,3,6,6,4,8,2]]))
